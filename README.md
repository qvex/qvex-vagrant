# QVEX Vagrant

Vagrant is a tool for automatically provisioning virtual machines (VMs). This repository contains a `Vagrantfile` which provisions a working dev environment for programming the vex robot using `vex-rt`. If you don't want to use a VM you can read `provision.sh` and try to setup everything manually on your own system.

## Quickstart

- if on windows, install git: https://git-scm.com/downloads
- install virtualbox: https://www.virtualbox.org/wiki/Downloads
- install vagrant: https://www.vagrantup.com/downloads

Once you have all of the above programs installed, launch a terminal (if you are on windows launch Git Bash).

Using the terminal type the following and press enter to "clone" this repository onto your machine.

```sh
git clone https://gitlab.com/qvex/qvex-vagrant.git
```

Next, change directory (cd) into the repository you just cloned.

```sh
cd qvex-vagrant
```

Finally instruct vagrant to provision and start your virtual machine. This command looks for a `Vagrantfile` in the current directory so it will only work if we are in the `qvex-vagrant` directory.

```sh
vagrant up
```

Provisioning takes a while (~15min on my system). Once it's complete you can visit http://localhost:8080 in your browser and you should be greeted with an IDE like this:

![](./img/ide.png)

## Uploading to the robot

**TODO**

## Tips

- Learn more about vagrant's features with `vagrant --help`.
- Put your VM to sleep using `vagrant suspend` and start it again later with `vagrant up`.
- Delete your VM entirely using `vagrant destroy`.
