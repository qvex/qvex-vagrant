#!/bin/bash

set -Eeuo pipefail

echo 'installing apt packages...'
sudo apt-get update -yq
sudo DEBIAN_FRONTEND=noninteractive apt-get -yq install \
	virtualbox-guest-utils build-essential gcc-arm-none-eabi clang python3-pip

echo 'installing rust...'
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
source "${HOME}/.cargo/env"
rustup component add rust-src

echo 'installing pros-cli...'
sudo python3 -m pip install pros-cli

echo 'installing code-server...'
sudo curl -fsSL https://code-server.dev/install.sh | sh
sudo systemctl enable --now code-server@vagrant
cat > ~/.config/code-server/config.yaml <<EOF
bind-addr: 0.0.0.0:8080
auth: none
cert: false
EOF
sudo systemctl restart code-server@vagrant
